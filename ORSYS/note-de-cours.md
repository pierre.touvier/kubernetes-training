# Formation Kubernetes (Orsys)

## Présentation des participants/formateur

* Team full Altran, full Orange:
  * Pierre TOUVIER
  * Guillaume MOTTE
  * Thomas BELIN
  * Marceau LE
  * Jean-François FELLA
  * Yannick REYNARD
  * Thomas TOGNACCI
  * Mehdi AHIZOUNE

* Formateur:
  * Pierre Yves DROIN

* Les attentes de la formation: Partagés par tous, volonté d'apprendre à administrer et un cluster K8s, le mettre en place.

## Plan de la formation

### 1. Introduction à K8s

#### Rappel

* Création d'un standart pour le format des containers : [OCI (Open Container Initiative)](https://opencontainers.org/)
* un container doit être minimal, légé, rapide
* devloppeur cré => push dans la registry => récupération par l'ops => deploiement quelque part ... -> "build once ... run anywhere"
* facilite les tests
* plus d'excuse du type "mais pourtant ça marchait chez moi"
* comparaison classique : virtualisation vs docker
![img](https://i.imgur.com/cMAwb4C.png)
  * un container ne virtualise pas, ils sont instanciés avec container runtime
  * un container n'émule pas le système

#### Qu'est-ce que K8s

* système permettant d'éxécuter et de coordonner les applications containerisées sur un cluster
* gère le life cycle des applications et service
* fonctionne en couple avec docker (vrai et faut car s'appuit sur le Container Runtime qui peut ne pas être docker)
* en 2017 Docker officialise k8s en tant qu'orchestrateur officiel
* K8s est créé par google
  * à l'origine borg, en interne chez google pour administrer les workloads
  * devenu open source
  * adopté par toute la communauté
* K8s est la nouvelle génération de container
* Docker est remplacé par [cri-o](https://cri-o.io/) pour le runtime de k8s

### 2. Les fichiers descriptifs

* fichier de description en yaml ou en json
* faire les premiers exos

### 3. Architect ure de K8s

* deux types : node worker et node master
* un node master (ou control plane) est composé de:
  * controller manager
  * scheduler
  * etcd (base de donnée)
  * API server (pour l'administration au quotidient)
  * loadbalancer (pour les exposition des applications)
* un node worker (ancienement minion ou encore compute)
  * controler runtime (gestion du container)
  * kubelet (donne les infos au master, dans quel état est le worker, met à jour l'etcd)
    kube-proxy (gestion de la partie réseau entre les worker)
* en résumé : (image)
* petite manipulation :
  * récupérer les noms courts: `kubectl api-resources`

#### Les objets de base dans k8s

* le Pod
  * la plus petite unité manipulé par k8s est le pod
  * un pod représente généralement un ou plusieurs conteneurs concernant une seul application
  * tous les containers d'un même pod partagent les mêmes stacks réseaux et disques (donc il faut mettre des ports différents par exemple)
* un pod peut être:
  * killed
  * les ips ne sont pas fixe
  * peuvent être scalled
  * bonne pratique: il faut doublé le pod (pour toujours en avoir un d'actif)
* le Deployment
  * c'est un objets permettant de lancer des Pods.
  * les bonnes pratiques demandent à utiliser les deployments.
  * solution simple, couplé avec le ReplicaSet, de faire du rolling update / blue-green update
  * permet de gérer l'historisation et donc de faciliter les roolback
* le Service (schématiquement la carte réseau du Pod)
  * permet d'affecter une ip fixe aux Pods
  * il communique le Port (qui peut être différent du port utilisé par l'applicatif)
  * il connait le targetPort qui est le port du POD sur lequel le service est exécuté
  * expose le Nodeport qui sera utilisé par le kube-proxy pour accéder de l'extèrieur à l'application

#### Le stockage

* possibilité de faire du stockage persistant:
  * utilisation du disque du cluster avec des montages comme docker
  * utilisation de NFS, glusterFS...
  * utilisation de stockage cloud (AWS, GCE...)
* différents type de montage:
  * read only...
  * ReadWriteOnce: un seul pod pourra écrire à la fois (utile pour une db mysql par exemple)
  * ReadWriteMany: plusieurs pod peuvent écrire en même temps

### 4. Exploiter K8s

#### Les namespaces

* moyen de séparer virtuellement les applications entre team/user...
* réelle bonne pratique
* si pas de namespace précisé, ca tombe dans default (donc les pods seront toujours dans un namespace)
* petit add créé par la communauté : `kubens` et `kubectx` pour switcher simplement de contexte et namespace

#### Mettre à jour un pod/application

* il faut mettre a jour le fichier de deployment puis faire un apply `kubectl apply -f <filename>`
* on peut vérifier que le pod est bien redéployé `kubectl get po,svc`

#### Exposer une application

* type d'exposition: loadbalancer, service, ingress...
* [un bon article à ce sujet](https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0)
* [ou encore celui là, plus spécific à Ingress](https://www.ovh.com/blog/getting-external-traffic-into-kubernetes-clusterip-nodeport-loadbalancer-and-ingress/)
* Ingress n'est pas installé automatiquement, il faut l'ajouter au cluster.
* il existe plusieurs types de coontroler ingress, par defaut `NGINX`, mais beaucoup mieux le `Traefic`

#### Gestion des secrets, manipulation des configs map

* les secrets sont encodés en base64
* on les stocks dans des configs maps, que l'on peut monter en tant que volume (donc pas de modification de l'image ou du pod)
* mise à jour du configMap pour changer les données, il faut re-apply le configMap mais aussi delete le Pod utilisant le CM!

#### Limiter les ressources

* request: ce qui est réservé à l'initialisation
* limite: ce qui ne pourra pas être dépassé
* possibiliter de forcer un quota si cela n'est pas définit dans le pod (`LimitRanger`)
* utilisation de `admissionControler` pour ne pas deployer si le quota demandé dépasse le quota aloué au namespace

#### Les healthchecks

* 2 types:
  * `liveness`: est-ce que le pod/container est en bonne santé, si non, il est restart
  * `readyness`: est-ce que le pod/container est prêt à recevoir du trafic
* les deux peuvent être définis pour un même pod

#### Stateful vs Stateless

* pour du stateless, on peut utiliser le controler `deployment`
* pour du stateful, une base de donnée par exemple, on ne peut pas car il n'y a pas de notion de priorité. Il faut donc utiliser le `StatefulSet`

#### DaemonSet

* la solution pour ajouter automatiquement des pods permettant la surveillance d'un noeud.
* (page 80 du cours)
* il ne peut avoir de replica sur un même noeud pour le daemonSet

#### Petit retour à la source: Docker

1. Création du Dockerfile
    * on part toujours d'une base (alpine pour que ce soit le plus petit, mais on prend ce qu'on veut)
    * on y définit un répertoire de travail: `WORKDIR <path from />`
    * si besoin on définit un user: `USER <mon user>`
    * on ajoute tout ce qu'on veut...
2. On fait le build: `docker build -t <tag> .`
3. On push l'image dans une registry: `docker push ...`
4. L'images est prête à être pull et utilisée

> les points 1, 2 et 3 sont une vision dev
>
> le point 4 est un vision ops

### 5. Gestion avancée de conteneurs

* upscale et downscale automatique avec HPA (page 67 du labs)
* gestion des accès avec RBAC (page 70 du labs)
  * plusieurs étapes:
    * authentication : user qui s'authentifie sur le cluster (page 70)
    * authorization : le user a le droit de faire l'action (page 73)
    * admission control : le cluster authorise l'action car il y a la ressource

> **Petite astuce** : arrêt d'une appli qui est gérée par un deployment : mettre le nombre de replica à 0

### 6. K8s en production

#### Notes importantes

* [copier un secret d'un namespace vers un autre](https://medium.com/@akila.senarath/how-to-copy-kubernetes-secrets-configmaps-between-namespaces-and-between-clusters-6e2d4d0f8bb8)
* [authentication for specific registry/dockerhub](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)

#### Rebalance pods after drain

* [voir le github ici](https://github.com/kubernetes-sigs/descheduler)

### 7. Déploiement d'un cluster K8s

## Annexe

### A. Support exercices

* le fichier pdf pour les LABs: kubernetes_labs.pdf
* le fichier pdf pour le cours: Support_de_cours_Kubernetes_ref_UBE.pdf

### B. Server anydesk

* numéro de machine: 728 608 903
* mot de passe: any-ubecv3
* user/passd: usera / usera
