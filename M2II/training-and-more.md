# KUBERNETES TRAINING

## [PDF de la formation](https://gitlab.com/pierre.touvier/kubernetes-training/-/blob/master/kubernetes-presentation.pdf)

## Support de cours et exercices

* [support](https://www.cncf.io)

Les exercices sont ici:

* [lien gitlab](https://gitlab.com/lucj/k8s-exercices)
* [site play with k8s](https://www.katacoda.com/courses/kubernetes/playground)
* [katacoda](https://www.katacoda.com/)

## Les outils utiles

* [minikube](https://github.com/kubernetes/minikube) ou un compte chez digitalocean
* [Kubens, kubectx](https://github.com/ahmetb/kubectx)
* [Prompt k8s assez utile](https://github.com/jonmosco/kube-ps1)
* kube-adm
* kind (kubernetes in docker) pour chez soi ou minikube avec une vm virtualbox

## Des cloud providers « alternatifs » qui ont de plus de très bons articles de blog

* [DigitalOcean](https://digitalocean.com)
* [Exoscale](https://exoscale.com)

## Solutions de sécurité et monitoring

* [aquasec](https://blog.aquasec.com)
* [instana](https://www.instana.com/)

## [Description de l’API de Kubernetes](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15)

## [Mise en place d’un cluster avec kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/)

## [Fonctionnement de Raft, alto de consensus distribué](http://thesecretlivesofdata.com/raft/)

## [Cloud Native Computing Foundation](https://cncf.io)

## Solution de ServiceMesh

* [istio](https://istio.io/)
* [linkerd](https://linkerd.io/)

## Storage

* [Open EBS](https://openebs.io/)
* [rook](https://rook.io)
* Et notamment un [exemple avec Cassandra](https://rook.github.io/docs/rook/v0.9/cassandra.html)

## [Comparaison des plugins réseau (Weave, Flannel, Calico, …)](https://www.objectif-libre.com/fr/blog/2018/07/05/comparatif-solutions-reseaux-kubernetes/)

## [Service vs Ingress ressources](https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0)

## [Infrastructure as Code](https://www.terraform.io/)

## [Kubernetes for IoT / edge](https://k3s.io/)

## [Exercices k8s](https://gitlab.com/lucj/k8s-exercices)

## [Blog de Luc JUGGERY](https://medium.com/@lucjuggery)
